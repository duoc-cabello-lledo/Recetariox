import React from "react";
import style from "./Recipe.module.css";

const Recipe = ({ title, calories, image, ingredients, label }) => {
  function saveRecipe() {
    console.log({ title }, { calories }, { image }, { ingredients }, { label });
  }

  return (
    <div className={style.recipe}>
      <h1>{title}</h1>

      <ol>
        {ingredients.map(ingredient => (
          <li>{ingredient}</li>
        ))}
      </ol>
      <button onClick={saveRecipe} className="add-button" type="submit">
        Guardar
      </button>
      <h2>{label}</h2>
      <p>Calories: {calories.toFixed(0)}</p>
      <img className={style.image} src={image} alt="" />
      <br></br>
    </div>
  );
};

export default Recipe;
